// newclock.cpp
// creates and maintains text files that contain both time and date (date can have a custom century added).
// can then be used as text file within the text source of your favourite streaming software
// recommend using a ramdisk to avoid causing harm to the health of your disks
#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <time.h>
using namespace std;

int main()
{
	// USER INPUT: put the directory in which you want the texts to be output. MUST HAVE TRAILING SLASH
	// NOTE TO WINDOWS USERS: you DO need to escape backslashes (\\). learned this the hard way
	string dir = "N:\\";
    time_t currentTime;
    struct tm * timeinfo;
	char b1[80];
    char b2[80];
	char b3[80];
	// change this to whatever you want the current century to be. NO RESPONSIBILITY TAKEN FOR MISMATCHING DAYS OF THE WEEK.
	const char *century = "44";

    while(true) {
        // get the actual current time and convert
        time(&currentTime);
		timeinfo = localtime(&currentTime);
		// outputs time in format [HH:MM:SS] (24h) to time.txt
        strftime(b1, sizeof(b1),"%H:%M:%S", timeinfo);
		ofstream timeTxt (dir + "time.txt", ofstream::trunc);
		timeTxt << b1;
		timeTxt.close();
		// outputs time in format [century][YY/MM/DD] to date.txt
		strftime(b2, sizeof(b2),"%m.%d", timeinfo);
		strftime(b3, sizeof(b3),"%y.", timeinfo);
		ofstream dateTxt (dir + "date.txt", ofstream::trunc);
		dateTxt << century << b3 << b2;
		dateTxt.close();
		// wait 500ms before updating again
        this_thread::sleep_for(chrono::milliseconds(500));
    }
    return 0;
}
