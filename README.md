```
  _             _                      ___ _____ 
 | |_ ___ _   _| |__   __ _ ___  __ _ ( _ )___ / 
 | __/ __| | | | '_ \ / _` / __|/ _` |/ _ \ |_ \ 
 | |_\__ \ |_| | |_) | (_| \__ \ (_| | (_) |__) |
  \__|___/\__,_|_.__/ \__,_|___/\__,_|\___/____/ 
```

配信素材リポジトリ                          
Stream Asset Repository
----
句の欠片だけで話す
speaking only in phrases

みんなへの恩返し
giving back to everyone

アタシの工程が分かれる物
to understand my own creation process

イラスト・アバター・プログラム・レイアウトなど
clip art, avatars, scripts, layouts etc.

（多分）役に立つ物の置き場
a place where i put stuff that is (hopefully) useful

（このリポジトリには、自分で作った素材と転載の許可を与えられた素材だけですが、フリー・無料でも転載が許可されない素材を含まずに他のテキストファイル参考することになります）
（著作権の問題にならないように、コピーレフト・CCなどのフリー素材だけです。素材に著作権で守っている部分がありましたら、その部分を消してメモを書いてから挙がります。）
(this repository contains only assets that i created myself or have explicit permission to share along. any third-party assets that i do not have permission to share along will be listed in a separate text file.)
(to prevent copyright issues, only copyleft, CC, or otherwise free-as-in-freedom assets will be contained. if an asset contains a copyrighted portion, it will be cleared (and noted) before being uploaded.)
